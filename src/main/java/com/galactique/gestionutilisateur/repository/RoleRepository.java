package com.galactique.gestionutilisateur.repository;

import com.galactique.gestionutilisateur.jpa.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
