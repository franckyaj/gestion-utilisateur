package com.galactique.gestionutilisateur.repository;

import com.galactique.gestionutilisateur.jpa.Privileges;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PrivilegeRepository extends JpaRepository<Privileges, Long> {
}
