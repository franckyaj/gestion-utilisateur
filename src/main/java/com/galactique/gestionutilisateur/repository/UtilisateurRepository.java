package com.galactique.gestionutilisateur.repository;

import com.galactique.gestionutilisateur.jpa.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {
}
