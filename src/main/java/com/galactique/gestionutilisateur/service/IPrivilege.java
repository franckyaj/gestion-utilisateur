package com.galactique.gestionutilisateur.service;

import com.galactique.gestionutilisateur.jpa.Privileges;

import java.util.List;

public interface IPrivilege {

    public Privileges save(Privileges p);
    public List<Privileges> findAll();
    public Privileges findOne(Long id);
}
