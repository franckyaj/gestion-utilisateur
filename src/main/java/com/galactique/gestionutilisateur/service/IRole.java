package com.galactique.gestionutilisateur.service;

import com.galactique.gestionutilisateur.jpa.Role;

import java.util.List;

public interface IRole {

    public Role save(Role r);
    public List<Role> findAll();
    public Role findOne(Long id);
    public boolean remove(Long id);
}
