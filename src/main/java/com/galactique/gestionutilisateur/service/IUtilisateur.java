package com.galactique.gestionutilisateur.service;

import com.galactique.gestionutilisateur.jpa.Utilisateur;

import java.util.List;

public interface IUtilisateur {

    public Utilisateur createUtilisateur(Utilisateur utilisateur);
    public Utilisateur updateUtilisateur(Utilisateur utilisateur);
    public Utilisateur initUtilisateur(Long id);
    public List<Utilisateur> filter(Utilisateur utilisateur);
    public boolean disableUtilisateur(Long id);

}
