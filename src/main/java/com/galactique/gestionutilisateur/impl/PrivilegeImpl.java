package com.galactique.gestionutilisateur.impl;

import com.galactique.gestionutilisateur.jpa.Privileges;
import com.galactique.gestionutilisateur.repository.PrivilegeRepository;
import com.galactique.gestionutilisateur.service.IPrivilege;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PrivilegeImpl implements IPrivilege {

    @Autowired
    PrivilegeRepository repository;

    @Override
    public Privileges save(Privileges p) {
        return repository.save(p);
    }

    @Override
    public List<Privileges> findAll() {
        return repository.findAll();
    }

    @Override
    public Privileges findOne(Long id) {
        return repository.findById(id).get();
    }
}
