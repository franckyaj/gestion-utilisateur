package com.galactique.gestionutilisateur.impl;

import com.galactique.gestionutilisateur.jpa.Role;
import com.galactique.gestionutilisateur.repository.RoleRepository;
import com.galactique.gestionutilisateur.service.IRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class RoleImpl implements IRole {

    @Autowired
    RoleRepository repository;

    @Override
    public Role save(Role r) {
        r.setDateCreated(new Date());
        return repository.save(r);    }

    @Override
    public List<Role> findAll() {
        return repository.findAll();
    }

    @Override
    public Role findOne(Long id) {
        return repository.findById(id).get();
    }

    @Override
    public boolean remove(Long id) {
        boolean deleted = false;
        Role r = repository.findById(id).get();
        try{
            repository.delete(r);
            deleted = true;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return deleted;    }
}
