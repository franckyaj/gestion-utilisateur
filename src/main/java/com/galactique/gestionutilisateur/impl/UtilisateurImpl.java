package com.galactique.gestionutilisateur.impl;

import com.galactique.gestionutilisateur.jpa.Utilisateur;
import com.galactique.gestionutilisateur.repository.UtilisateurRepository;
import com.galactique.gestionutilisateur.service.IUtilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UtilisateurImpl implements IUtilisateur {

    @Autowired
    UtilisateurRepository repository;
    @Autowired
    EntityManager em;

    @Override
    public Utilisateur createUtilisateur(Utilisateur u) {
        u.setFirstName(u.getFirstName().toUpperCase());
        u.setLastName(u.getLastName().toUpperCase());


        if(u.getId() == null) {
            u = this.createLogin(u);
            u.setDateCreated(new Date());
            u.setDefaultUser(true);
        }
        else if(u.getUsername().equals("ADMIN")) {

        }
        else {
            Utilisateur user = repository.findById(u.getId()).get();
            u.setDateModified(new Date());
            u.setPassword(user.getPassword());
            u.setPwdSalt(user.getPwdSalt());
            u.setUserCreated(user.getUserCreated());
            u.setDefaultUser(user.isDefaultUser());
        }
        if(u.getDefaultPassword()!= null) u.setPassword(null);
        return repository.save(u);
    }

    @Override
    public Utilisateur updateUtilisateur(Utilisateur utilisateur) {
        return null;
    }

    @Override
    public Utilisateur initUtilisateur(Long id) {
        return null;
    }

    @Override
    public List<Utilisateur> filter(Utilisateur utilisateur) {
        return null;
    }

    @Override
    public boolean disableUtilisateur(Long id) {
        return false;
    }

    /*
     * this function create a user login
     */
    public Utilisateur createLogin(Utilisateur u) {
        int i = 0;
        if(u.getFirstName().length() > 3) {
            String login =u.getFirstName().substring(0,4).toUpperCase();
            System.out.println("login " + login);
            while(checkLoginexist(login)) {
                login =u.getFirstName().substring(0,4).toUpperCase() + i;
                System.out.println("new login " + login);
                i++;
            }
            u.setUsername(login);
        }
        else {
            String login =u.getFirstName().substring(0,3).toUpperCase()+0;
            //System.out.println("login " + login);
            while(checkLoginexist(login)) {
                login =u.getFirstName().substring(0,3).toUpperCase() + i;
                System.out.println("new login " + login);
                i++;
            }
            u.setUsername(login);

        }
        return u;
    }

    /*
     * this function check if the login already exist in database
     */
    public boolean checkLoginexist(String login) {
        Utilisateur u = new Utilisateur();
        u.setUsername(login);
        List<Utilisateur> users = filterUser(u);
        if(users.size() == 0) {
            return false;
        }
        else {
            return true;
        }
    }

    public List<Utilisateur> filterUser(Utilisateur u) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Utilisateur> cq = cb.createQuery(Utilisateur.class);

        Root<Utilisateur> user = cq.from(Utilisateur.class);
        List<Predicate> predicates = new ArrayList<>();

        if(u.getUsername() != null) {
            predicates.add(cb.equal(user.get("login"), u.getUsername()));
        }

        cq.where(predicates.toArray(new Predicate[0]));
        return em.createQuery(cq).getResultList();

    }
}
