package com.galactique.gestionutilisateur.jpa;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name="ROLE")
public class Role implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String roleName;
    private String description;
    @ElementCollection
    @CollectionTable(name = "data")
    private List<String> privileges;
    private boolean active;
    @Temporal(TemporalType.DATE)
    private Date dateCreated;
    @Temporal(TemporalType.DATE)
    private Date dateModified;
    private String userModified;
    private String userCreated;
    @ManyToMany(mappedBy="roles")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Utilisateur> utilisateurs;
}
