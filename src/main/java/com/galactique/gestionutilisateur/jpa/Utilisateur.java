package com.galactique.gestionutilisateur.jpa;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name="UTILISATEUR")
public class Utilisateur implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private String defaultPassword;
    private boolean defaultUser = false;
    private String phoneNumber;
    private String email;
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;
    @Temporal(TemporalType.DATE)
    private Date dateCreated = new Date();
    @Temporal(TemporalType.DATE)
    private Date dateModified;
    private String pwdSalt;
    private String userModified;
    private String userCreated;
    private boolean active = true;
    @ManyToMany
    @JoinTable(
            name = "User_Role",
            joinColumns=@JoinColumn(name="userID"),
            inverseJoinColumns=@JoinColumn(name="RoleId"))
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Role> roles;
}
